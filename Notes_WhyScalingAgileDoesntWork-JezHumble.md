# Outline & notes for GOTO 2015 - Why Scaling Agile Doesn't Work - Jez Humble

## Water-Scrum-Fall

## Tiny little bits worked by teams

## Use Cost of Delay to prioritize work based on dollar value

## Impact Mapping

## Hypothesis Driven Delivery

## HP Laserjet Case Study

### Note that it took 3 to 4 years (2008 to 2011) to complete

### HP Goals

Step 0: Understand the goals in measurable terms

1. HP **did not** create a three year plan
2. HP started with the following two goals:
   * Get firmware off of the critical Path
   * 10X productivity improvement
   * Note: The two goals are **measurable** and **easy to understand**